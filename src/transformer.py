from mxnet.gluon.data.vision import transforms


class Transformer:
    """Runs transformation pipelines for dataset, running augmentation only on base files

    Attributes:
        data (dataset): Selected dataset used for transformations
        shape (int or tuple of (W, H), optional): Shape of reshaped dataset if given.
            Defaults to None.
        transform_list (List of str, optional): List of gluon transform functions to be used.
            Defaults to None.
    """

    def __init__(self, data, shape=None, transform_list=None):
        self.data = data
        self.shape = shape
        self.transform_list = transform_list

    def run(self, **kwargs):
        """Runs the transformer

        Args:
            kwargs: named arguments for transformation functions if given

        Returns:
            transformed_data (Dataset): Dataset with all the transformations applied.
        """
        transformed_data = self._transform_all()

        if self.transform_list:
            transformed_data = self._transform_base(data=transformed_data, **kwargs)

        transformed_data = transformed_data.transform(transforms.Normalize(0, 1))

        return transformed_data

    def _transform_all(self):
        """Runs core transformations to entire dataset such as toTensor and Resize

        Returns:
            transformed_data (Dataset): Dataset with core transformations.
        """
        pipeline = [transforms.ToTensor()]

        if self.shape:
            pipeline.insert(0, transforms.Resize(self.shape))

        composed_transformer = transforms.Compose(pipeline)
        transformed_data = self.data.transform(composed_transformer)

        return transformed_data

    def _transform_base(self, data, **kwargs):
        """Runs data augmentations on base data only.

        Args:
            data (Dataset): Dataset object used to apply transformations on.
            kwargs: Arguments for transformation functions.

        Returns:
            transformed_data (Dataset): Dataset with augmentations applied.
        """
        pipeline = []

        if self.transform_list:
            pipeline = [
                getattr(transforms, method)(**kwargs)
                if hasattr(transforms, method)
                else print(f"{method} not available.")
                for method in self.transform_list
            ]

        composed_transformer = transforms.Compose(pipeline)
        transformed_data = data.transform_first(composed_transformer)

        return transformed_data
