from collections import deque
from os import listdir
from pathlib import Path
from shutil import rmtree
from zipfile import ZipFile

import numpy as np
from PIL import Image


class DataProcessFactory:
    """Sets up and process required data examples/sets.

    Attributes:
        process_type (str) If processor is set to a training dataset.
        dataset (str): Name of processor's dataset.
        data_path (Path): Path of dataset root.
        data_temp_path (Path): Path of temp data root.
    """

    def __init__(
        self, *, stage: int, is_train=True, is_val=False, train_test_split=0.2
    ):
        """
        Args:
            stage (int): Determines what stage dataset is from.
            is_train (bool, optional): If dataset is a training set.
                Defaults to True.
            is_val (bool, optional): Is data being used as a validation set.
                Defaults to False.
            train_test_split (float, optional): Ratio of training set used for validation.
                Defaults to 0.2
        """
        stage = f"stage{stage}"

        self.process_type = "train" if is_train else "test"
        self.dataset = f"{stage}_{self.process_type}"
        self.data_temp_path = Path(f"../temp/{self.dataset}")
        self.is_val = is_val
        self.train_test_split = train_test_split

        if is_val:
            self.dataset = f"{stage}_val"

        self.data_path = Path(f"../data/{self.dataset}")

    def run(self, random_seed=42, reformat=False):
        """Runs the ProcessFactory. Wrapper Function for private functions.

        setup_directories -> setup_data

        Args:
            reformat (bool, optional): Ignore warnings and rerun processor.
                Defaults to False.
            random_seed (int, optional): Seed used for train & validation split.
                Defaults to 42.

        """
        np.random.seed(random_seed)

        processed_datasets_file = Path("../temp/processed.txt")
        skip_processing = False

        try:
            processed_datasets_file.read_text()
        except FileNotFoundError:
            processed_datasets_file.parent.mkdir(exist_ok=True)
            processed_datasets_file.touch()
        finally:
            processed_datasets = processed_datasets_file.read_text()

        if self.dataset in processed_datasets:
            if not reformat:
                skip_processing = True

        if not skip_processing:
            self.__setup_directories()
            self.__setup_data()

            with processed_datasets_file.open("a") as processed_list_file:
                processed_list_file.write(f"{self.dataset}\n")

    def __setup_directories(self):
        if not self.data_path.exists():
            self.data_path.mkdir(parents=True)

        data_subdirs = ["images", "masks"]
        for subdir in data_subdirs:
            subdir_path = self.data_path / subdir
            if subdir_path.exists():
                rmtree(subdir_path)

            if subdir == "images":
                subdir_path.mkdir()
            elif subdir == "masks" and self.process_type == "train":
                subdir_path.mkdir()

    def __setup_data(self):
        print(f"\nSetting up data files for {self.dataset}.")

        data_zip = Path(f"../temp/{self.dataset}.zip")
        self.data_temp_path.mkdir(exist_ok=True)

        try:
            with data_zip.open("rb") as file:
                zip_file = ZipFile(file)
                zip_file.extractall(path=self.data_temp_path)
        except FileNotFoundError:
            if self.is_val:
                data_zip = Path(f"../temp/{self.dataset.replace('val', 'train')}.zip")
            else:
                raise FileNotFoundError(
                    f"Zip file for {self.dataset} not found in temp/ folder!"
                )
        finally:
            with data_zip.open("rb") as file:
                zip_file = ZipFile(file)
                zip_file.extractall(path=self.data_temp_path)

        self.__organize_data()
        rmtree(self.data_temp_path)
        print(f"Finished setting up data files for {self.dataset}.\n")

    def __organize_data(self):
        image_files = self.data_temp_path.rglob("images/*.png")

        dataset_size = len(listdir(str(self.data_temp_path)))
        inds = np.arange(dataset_size)

        if self.process_type == "train":
            train_size = int(dataset_size * (1.0 - self.train_test_split))
            inds = np.random.choice(dataset_size, train_size, False)

            if self.is_val:
                inds = np.setdiff1d(np.arange(dataset_size), inds)

        if self.process_type == "train":
            mask_directories = self.data_temp_path.rglob("masks/")
            formatted_images = self.__format_data(paths=image_files, split_inds=inds)
            masks = self.__format_data(
                src="masks", split_inds=inds, paths=mask_directories
            )

            deque(masks, maxlen=0)
        else:
            formatted_images = self.__format_data(paths=image_files, split_inds=inds)

        deque(formatted_images, maxlen=0)

    def __format_data(self, paths, split_inds, src="images"):
        """Formats data files to correct format.

        Changes file names to ordered numerical values, creates single mask/gd_truth image.

        Args:
            paths (generator<Path>): Generator of files' path to be formatted.
            split_inds (List of ints): Indices used for splitting datasets.
            src (str): Type of data being formatted.
                Defaults to 'images'.

        Returns:
            formatted_files (generator): output files with corrected format.
        """
        destination_folder = self.data_path / "images"
        formatted_files = (
            path.rename(destination_folder / f"{ind:04}.png")
            for ind, path in enumerate(paths)
            if ind in split_inds
        )

        if src == "masks":
            destination_folder = self.data_path / "masks"
            formatted_files = (
                self.generate_mask(ind, path, destination_folder)
                for ind, path in enumerate(paths)
                if ind in split_inds
            )

        return formatted_files

    @staticmethod
    def generate_mask(ind: int, path: Path, destination_folder: Path):
        """Combine multiple mask images into one ground truth file.

        Args:
            ind (int): Current file index.
            path (Path): Path to dir containing masks.
            destination_folder (Path): Path of formatted file destination.
        """
        mask_images = path.glob("*png")
        mask_path = destination_folder / f"{ind:04}.png"
        decoded_masks = [np.asarray(Image.open(image)) for image in mask_images]
        combined_mask = np.sum(decoded_masks, axis=0)

        ground_truth = Image.fromarray(combined_mask.astype("uint8"), "L")
        ground_truth.save(mask_path)
