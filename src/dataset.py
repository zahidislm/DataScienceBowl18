from pathlib import Path

from mxnet.gluon.data.dataset import Dataset, _LazyTransformDataset
from mxnet.image import imread


def patched_getitem(self, idx):
    """Patched __getitem__ for Dataset'that allows usage of gluon's transform functions
    instead of needing to use lambdas"""
    item = self._data[idx]

    if isinstance(item, tuple):
        transformed_base = self._fn(item[0])
        transformed_mask = self._fn(item[1])
        return transformed_base, transformed_mask

    return self._fn(item)


# Monkey-patching :(
_LazyTransformDataset.__getitem__ = patched_getitem


class CellDataset(Dataset):
    """Dataset for cell images that packs both base and mask images

    Attributes:
        root (Path): Path to Dataset files
        is_train (bool): Packs masks if true.
    """

    def __init__(self, root: Path, is_train: bool):
        self.root = root
        self.is_train = is_train
        self.images = list(root.glob("images/*.png"))

        if is_train:
            self.masks = list(root.glob("masks/*.png"))

    def __getitem__(self, idx):
        base_path = self.images[idx]
        base = imread(str(base_path))

        if self.is_train:
            mask_path = self.masks[idx]
            mask = imread(str(mask_path))

            return base, mask

        return base

    def __len__(self):
        return len(self.images)
